add-apt-repository -y ppa:ondrej/php && \
apt update -yq && apt upgrade -yq && \
apt install -yq \
    nginx \
    nginx-extras \
    mariadb-server \
    redis-server \
    memcached \
    postfix \
    apache2-utils \
    gnuplot \
    nodejs \
    npm \
    php8.1 \
    php8.1-bcmath \
    php8.1-bz2 \
    php8.1-cgi \
    php8.1-cli \
    php8.1-common \
    php8.1-curl \
    php8.1-dba \
    php8.1-dev \
    php8.1-enchant \
    php8.1-fpm \
    php8.1-gd \
    php8.1-gmp \
    php8.1-imap \
    php8.1-interbase \
    php8.1-intl \
    php8.1-ldap \
    php8.1-mbstring \
    php8.1-mysql \
    php8.1-odbc \
    php8.1-opcache \
    php8.1-pgsql \
    php8.1-phpdbg \
    php8.1-pspell \
    php8.1-readline \
    php8.1-snmp \
    php8.1-soap \
    php8.1-sqlite3 \
    php8.1-sybase \
    php8.1-tidy \
    php8.1-xml \
    php8.1-xmlrpc \
    php8.1-xsl \
    php8.1-zip \
    libapache2-mod-php8.1 \
    libphp8.1-embed \
    php-pear && \
dpkg --purge apache2 && \
    apt purge apache2 && \
    apt remove apache2 && \
    rm -f /etc/init.d/apache2 && \
pear install Net_GeoIP && \
npm install pm2 -g && \
apt remove libnginx-mod-http-perl -y && \
apt-mark manual libnginx-mod-http-geoip2 && \
apt autoremove -y && apt autoclean -y && \
addgroup webmasters && usermod -a -G webmasters www-data && \
mkdir /home/$USER/public_html && \
mkdir -p /home/$USER/public_html/vhost.com/{backup,log,public} && \
chown -R $USER:webmasters /home/$USER/public_html && \
chmod -R g+w /home/$USER/public_html && \
find /home/$USER/public_html -type d -exec chmod g+s {} \; && \
echo -e "\e[31mFINISHED :)\e[0m\n" && \
echo -e "* To install mcrypt for PHP, run: pecl install mcrypt-1.0.2" && \
echo -e "* To secure your MySQL, run: mysql_secure_installation\n" && \
echo -e "Lastly, refer to the webconfig repository for nginx/php configuration"