alias python=python3
alias pip=pip3

alias mkdir='mkdir -pv'  # make dir with all sub-directories
alias grep='grep --color=auto'
alias now='date +"%d-%m-%Y %T"'
alias df='df -h'
alias du='du -h -d 2'

# Fix last command
alias fuck='sudo $(history -p \!\!)'

# transfer.sh
transfer() {
    curl -F 'file=@'$1 https://0x0.st;
}

# find in file
fif() {
    if [  -z "$2" ]
    then
        echo "Missing argument: Use fif [what] [where]"
    else
        grep -inr $1 $2 | cut -d: -f1,2
    fi
}

mossh() {
    eval "mosh $1 --ssh='ssh ${2/-p/-p }'"
}

# VNC server
alias vncstart='vncserver :1 -geometry 1200x800'
alias vncstop='vncserver -kill :1'

# Common shell functions
alias llm="ls -lhF --group-directories-first --color=always | awk '{k=0;for(i=0;i<=8;i++)k+=((substr(\$1,i+2,1)~/[rwx]/) *2^(8-i));if(k)printf(\"%0o \",k);print}'"
alias ll="ls -lhF --group-directories-first --color=auto"
alias la="ls -lahF --group-directories-first --color=auto"
alias l="ls -laF | awk '{k=0;for(i=0;i<=8;i++)k+=((substr(\$1,i+2,1)~/[rwx]/) *2^(8-i));if(k)printf(\"%0o \",k);print}'"
alias tf='tail -f'
alias lh='ls -alt | head' # see the last modified files
alias lg='ll | grep' # show files matching "ls grep"

alias screen='TERM=screen screen'
alias cl='clear'
alias ka9='killall -9'
alias k9='kill -9'

# Zippin
alias gz='tar -zcvf'
alias ungz='tar -zxvf'

# NGINX
alias nxstart="sudo service nginx start"
alias nxstop="sudo service nginx stop"
alias nxrestart="sudo service nginx restart"
alias nxreload="sudo service nginx reload"
alias nxstatus="service nginx status"
alias nxconf="sudo service nginx configtest"
alias nxtest="sudo nginx -t"
alias nxdir="cd /etc/nginx"

# TMUX
alias tmx="tmux new -s"
alias tmxc="nano ~/.tmux.conf"
alias tmxa="tmux attach -t"
alias tmxk='tmux kill-session -t'
alias tmxl="tmux ls"

# GIT
alias gs='git status'
alias gstsh='git stash'
alias gst='git stash'
alias gsp='git stash pop'
alias gsa='git stash apply'
alias gsh='git show'
alias gshw='git show'
alias gshow='git show'
alias gi='vim .gitignore'
alias gcm='git ci -m'
alias gcim='git ci -m'
alias gci='git ci'
alias gco='git co'
alias gcp='git cp'
alias ga='git add -A'
alias guns='git unstage'
alias gunc='git uncommit'
alias gm='git merge'
alias gms='git merge --squash'
alias gam='git amend --reset-author'
alias grv='git remote -v'
alias grr='git remote rm'
alias grad='git remote add'
alias gr='git rebase'
alias gra='git rebase --abort'
alias ggrc='git rebase --continue'
alias gbi='git rebase --interactive'
alias gl='git l'
alias glg='git l'
alias glog='git l'
alias co='git co'
alias gf='git fetch'
alias gfch='git fetch'
alias gd='git diff'
alias gb='git b'
alias gbd='git b -D -w'
alias gdc='git diff --cached -w'
alias gpub='grb publish'
alias gtr='grb track'
alias gpl='git pull'
alias gplr='git pull --rebase'
alias gps='git push'
alias gpsh='git push -u origin `git rev-parse --abbrev-ref HEAD`'
alias gnb='git nb' # new branch aka checkout -b
alias grs='git reset'
alias grsh='git reset --hard'
alias gcln='git clean'
alias gclndf='git clean -df'
alias gclndfx='git clean -dfx'
alias gsm='git submodule'
alias gsmi='git submodule init'
alias gsmu='git submodule update'
alias gt='git t'
alias gbg='git bisect good'
alias gbb='git bisect bad'


