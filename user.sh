export PASSWORD=`mkpasswd -m sha-512 -s $PASSWORD` && \
useradd -m -p $PASSWORD -g sudo -s /bin/bash $USER && \
cp -R ~/.ssh /home/$USER/.ssh && \
cp ~/{.bash_aliases,.bash_logout,.bash_profile,.nanorc,.tmux.conf} /home/$USER/ && \
chown -R $USER:sudo /home/$USER && \
echo -e "\n\e[31mFINISHED :)\e[0m"
