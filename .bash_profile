if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f ~/.bash_logout ]; then
    . ~/.bash_logout
fi

alias aptupgrade="sudo apt update && sudo apt upgrade -y && apt autoremove -y && apt autoclean -y"

export EDITOR="/bin/nano"
export VISUAL="/bin/nano"

# Nicer promot (git aware)
export PS1="╭─\[\033[36m\]\u@\h\[\033[m\] \[\033[33;1m\]\w\[\033[m\] \[\033[35;1m\]\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)\[\033[m\]\n╰─\$ "
