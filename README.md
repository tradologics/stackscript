# Stack Script
### Initial server setup for Ubuntu 20.04 LTS

To get started, follow these steps:

1. Get this Repository Zip's URL from [here](https://bitbucket.org/tradologics/stackscript/downloads)
2. SSH to your freshly deployed server
3. Set the variables using the following commands (replace `<val>` with the desired values):


```
export USER=<username>
export PASSWORD=<user password>
export HOSTNAME=<hostname>
export SSHPORT=<ssh port>
export REPOURL=<repo zip url>
```

Then, run the stach script using this command:

```
apt install -yq unzip && cd && curl $REPOURL --output rf.zip && unzip -j rf.zip && rm README.md rf.zip && bash init.sh
```

### Optional steps:

- To set up a new user , run: `bash user.sh`
- If you're looking to use this server as a Web server, run: `bash websertver.sh`. Then, refer to the [webconfig](https://bitbucket.org/tradologics/webconfig) repository for specific Nginx/PHP configutation.